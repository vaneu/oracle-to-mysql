package test;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;

import java.io.StringReader;

/**
 * @author Vaneu
 */
public class JSQLParserTest {

    public static void main(String[] args) throws JSQLParserException {
        String sql = "SELECT A.a,B.a FROM A,B WHERE A.b = B.b(+)";
        Statement stat = CCJSqlParserUtil.parse(sql);
        System.out.println(stat);
    }
}
