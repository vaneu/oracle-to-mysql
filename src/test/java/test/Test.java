package test;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.oracle.parser.OracleStatementParser;
import com.alibaba.druid.sql.transform.SQLUnifiedVisitor;
import cn.rcsit.sql.transform.mysql.visitor.OracleToMySqlOutputVisitor;
import dm.jdbc.util.OracleDateFormat;

import java.io.StringWriter;


public class Test {

    //    private static String sql = "SELECT t.id,t.name,t.remark,TO_DATE(t.CREATE_TIME, 'yyyy-MM-dd') dd FROM TEST t WHERE TO_DATE(t.CREATE_TIME, 'yyyy-MM-dd') < TO_DATE(SYSDATE, 'yyyy-MM-dd')";
    private static String sql = "select to_char(sysdate,'yyyy') as nowYear   from dual; ";

    public static void main(String[] args) {

        OracleStatementParser parser = new OracleStatementParser(sql);

        // 使用parser解析生成ast，这里stat就是ast
        SQLStatement stat = parser.parseStatement();

        SQLUnifiedVisitor sqlUnifiedVisitor = new SQLUnifiedVisitor();
        stat.accept(sqlUnifiedVisitor);

        StringWriter out = new StringWriter();
        OracleToMySqlOutputVisitor oracleToMySqlOutputVisitor = new OracleToMySqlOutputVisitor(out, true);
        stat.accept(oracleToMySqlOutputVisitor);

        System.out.println("---||-------------------------------------------------------------------------------------");
        System.out.println(sql);
        System.out.println(out.toString());

    }
}
